#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <chrono>

using namespace std;
using namespace std::chrono;


enum METHOD {
    BF, BNB, BOTH
};

class DecideKnapsackProblem {

private:
    const long long id, size, capacity, minPrice, maxPrice;
public:
    long long getId() const;

    long long getSize() const;

    long long getBfSteps() const;

    long long getBnbSteps() const;

private:
    long long bfSteps = 0, bnbSteps = 1, currMaxPrice = -1;
    bool solution = false;
    vector<pair<long long, long long>> items;
public:
    DecideKnapsackProblem(const long long _id, const long long _size, const long long _capacity,
                          const long long _min_price, const long long _max_price,
                          const vector<pair<long long, long long>> &_items)
            : id(_id), size(_size), capacity(_capacity), minPrice(_min_price), maxPrice(_max_price),
              items(_items) {};

    pair<bool, long long> bfSolve();

    pair<bool, long long> bnbSolve();

    bool bfSolve(long long n, long long curr_weight, long long curr_price);

    bool bnbSolve(long long n, long long curr_weight, long long curr_price, long long price_to_add);

    void setSolution(long long price) {
        solution = price >= minPrice;
    }

    bool getSolution();


};

pair<bool, long long> DecideKnapsackProblem::bfSolve() {
    return {bfSolve(0, 0, 0), bfSteps};
}

bool DecideKnapsackProblem::bfSolve(long long n, long long curr_weight, long long curr_price) {
    if (n == size) {
        bfSteps++;
        return curr_weight <= capacity && curr_price >= minPrice;
    }
    return bfSolve(n + 1, curr_weight + items[n].first,
                   curr_price + items[n].second) ||
           bfSolve(n + 1, curr_weight, curr_price);

}

bool DecideKnapsackProblem::bnbSolve(long long n, long long curr_weight, long long curr_price, long long price_to_add) {
    if (n == size) {
        bnbSteps++;
        if (curr_price > currMaxPrice && curr_weight < capacity)
            currMaxPrice = curr_price;
        return curr_weight <= capacity && curr_price >= minPrice;
    }
    if (curr_weight > capacity || curr_price + price_to_add < minPrice || curr_price + price_to_add < currMaxPrice) {
        bnbSteps++;
        return false;
    }
    return bnbSolve(n + 1, curr_weight + items[n].first,
                    curr_price + items[n].second, price_to_add - items[n].second) ||
           bnbSolve(n + 1, curr_weight, curr_price, price_to_add - items[n].second);
}

pair<bool, long long> DecideKnapsackProblem::bnbSolve() {
    return {bnbSolve(0, 0, 0, maxPrice), bnbSteps};
}

bool DecideKnapsackProblem::getSolution() {
    return solution;
}

long long DecideKnapsackProblem::getId() const {
    return id;
}

long long DecideKnapsackProblem::getSize() const {
    return size;
}

long long DecideKnapsackProblem::getBfSteps() const {
    return bfSteps;
}

long long DecideKnapsackProblem::getBnbSteps() const {
    return bnbSteps;
}

DecideKnapsackProblem parseProblem(string &problem) {
    stringstream str(problem);
    long long id, n, capacity, min_price, max_price = 0, w, p;
    vector<pair<long long, long long>> items;
    str >> id >> n >> capacity >> min_price;
    for (size_t i = 0; i < n; ++i) {
        str >> w >> p;
        items.emplace_back(w, p);
        max_price += p;
    }
    return {id * (-1), n, capacity, min_price, max_price, items};

}

void parseSol(string &line, vector<DecideKnapsackProblem> &problems) {
    stringstream str(line);
    long long id, _, price;
    str >> id >> _ >> price;
    problems[id - 1].setSolution(price);

}

vector<DecideKnapsackProblem> readFile(string &filename) {
    ifstream file;
    file.open(filename);
    string line;
    vector<DecideKnapsackProblem> vec;

    if (file.is_open()) {
        while (getline(file, line)) {
            vec.push_back(parseProblem(line));
        }
        file.close();
    }
    return vec;
}

void readSols(string &filename, vector<DecideKnapsackProblem> &problems) {
    ifstream file;
    file.open(filename);
    string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            parseSol(line, problems);
        }
        file.close();
    }
}

void solveProblems(long long n, char type, ofstream &out, METHOD m) {
    string prob = "data/";
    prob += type;
    prob += "R" + to_string(n) + "_inst.dat";

    string sol = "data/";
    sol += type;
    sol += "K" + to_string(n) + "_sol.dat";


    auto problems = readFile(prob);
    readSols(sol, problems);

    for (auto p: problems) {
        auto start = high_resolution_clock::now();
        auto stop = high_resolution_clock::now();
        auto bfDuration = duration_cast<microseconds>(stop - start);
        pair<bool, long long> res = {true, 0};
        if (m == BF || m == BOTH) {
            auto res = p.bfSolve();
            stop = high_resolution_clock::now();
            bfDuration = duration_cast<microseconds>(stop - start);

            if (res.first != p.getSolution())
                throw logic_error("Oopsie woopsie");
        }

        stop = high_resolution_clock::now();
        auto bnbDuration = duration_cast<microseconds>(stop-stop);
        pair<bool, long long> res2 = {true, 0};
        if (m == BNB || m == BOTH) {
            start = high_resolution_clock::now();
            auto res2 = p.bnbSolve();
            stop = high_resolution_clock::now();
            bnbDuration = duration_cast<microseconds>(stop - start);

            if (res2.first != p.getSolution())
                throw logic_error("Oopsie woopsie");
        }

        out << p.getId() << "," << p.getSize() << "," << type << "," << bfDuration.count() << "," << bnbDuration.count()
            << ","
            << res.second << "," << res2.second << endl;
    }

}


int main() {
    long long sizes[] = {32, 35, 37, 40};
    auto type = 'N';
    ofstream out;
    out.open("outbnb.csv");
    METHOD m = BNB;

    out << "id,n,type,bruteTime,bnbTime,bruteConfigurations,bnbConfigurations" << endl;

    for (auto s: sizes) {
        cout << "Starting size " << s << endl;
        solveProblems(s, type, out, m);
    }

    out.close();
    return 0;
}