//
// Created by lucie on 23.12.2021.
//

#include <algorithm>
#include <random>
#include <utility>
#include "SAT.h"

bool SAT::frozen(long long int curr_temp) {
    return curr_temp < 1;
}

bool SAT::better(long long int new_solution, long long int old_solution) {
    return new_solution > old_solution;
}

ld SAT::cool(long double curr_temp, long double cooling_factor) {
    return curr_temp * cooling_factor;
}

bool SAT::equilibrium(long long int n) const {
    return n > equilibriumBound;
}

vector<ll> SAT::random_permutation(long long int size) {
    vector<ll> v(size);

    for (ll i = 0; i < size; ++i)
        v[i] = i;

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    return v;
}

SAT::SAT(int l, int c) : variablesCount(l), clauses_count(c) {}

void SAT::addClause(int a, int b, int c) {
    clauses.insert(Clause({a, b, c}));
}

bool SAT::isSat(vector<bool> &x) const {
    for (const auto &clause: clauses)
        if (!clause.isSat(x))
            return false;
    return true;
}

void SAT::setWeights(vector<int> w) {
    weights = std::move(w);
}

pair<ll, vector<bool>> SAT::update(const pair<long long int, vector<bool>> &curr_configuration) const {

    vector<bool> x = curr_configuration.second;

    if (rand() % 20 != 0)
        if (isSat(x)) {
            for (int i = 0; i < max(3, rand() % (variablesCount / 3)); ++i)
                x[1 + rand() % variablesCount].flip();
        } else {
            vector<ll> not_sat;
            for (auto &clause: clauses) {
                if (!clause.isSat(x))
                    for (auto l: clause.variables)
                        not_sat.push_back(max(l, -l));
            }
            x[not_sat[rand() % not_sat.size()]].flip();
        }
    else {
        for (auto a = 1; a <= variablesCount; ++a) {
            x[a] = rand() % 2 == 0;
        }
    }

    ll weight = 0;

    for (auto i = 1; i <= variablesCount; ++i)
        weight += x[i] * weights[i];

    weight -= isSat(x) ? 0 : weightsSum + 1;

    return {weight, x};
}

void SAT::setWeightsSum(ll weights_sum) {
    SAT::weightsSum = weights_sum;
}

int SAT::getSize() {
    return variablesCount;
}

void SAT::setEquilibriumBound(long long int equilibriumBound) {
    SAT::equilibriumBound = equilibriumBound;
}

ll SAT::getWeight(int n) const {
    if(n>weights.size())
        throw out_of_range("");
    return weights[n];
}

const set<Clause> &SAT::getClauses() const {
    return clauses;
}

Clause::Clause(const set<int> &literals) : variables(literals) {}

bool Clause::operator<(const Clause &rhs) const {
    return variables < rhs.variables;
}

bool Clause::operator>(const Clause &rhs) const {
    return rhs < *this;
}

bool Clause::operator<=(const Clause &rhs) const {
    return !(rhs < *this);
}

bool Clause::operator>=(const Clause &rhs) const {
    return !(*this < rhs);
}

bool Clause::isSat(vector<bool> &x) const {
    for (const auto &i: variables)
        if (i > 0 ? x[i] : !x[-i])
            return true;
    return false;
    //return any_of(variables.begin(), variables.end(), [&](int i) { return i > 0 ? x[i] : !x[-i]; });
}


