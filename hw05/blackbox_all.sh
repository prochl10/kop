RUNS=10

for type in data/*/
  do

  for folder in ${type}*/
  do

    if [ -d "out_${folder}" ]
    then
      echo "Skipping $folder"
      continue
    fi
    echo "Starting $folder"
    mkdir "out_${folder}"

    MAINOUTFILE="out_${folder}/main.csv"

    echo "file,meanIter,meanPrice,bestPrice,meanTime" > $MAINOUTFILE

    for file in ${folder}*
    do
      #echo "$file"

      #Getting opt
      prefix=`basename $file`
      prefix="${prefix%.mwcnf}" #eats mwcnf
      prefix="${prefix#w}" #eat w

      outfile="out_${file%.mwcnf}_out.csv"


      echo "iters,price,time" > $outfile

      #opt=`grep -w -m 1 "^${prefix}" "$OPTFILE" | awk '{print $2}'`
      totaltime=0
      totalit=0
      totalprice=0
      totaleps=0
      best=-100000000

      for i in $(seq 1 $RUNS)
      do
        start=$(date +"%s.%N")
        res=`./SA "$file"  | tail -1 | cut -d "," -f 1,2`
        end=$(date +"%s.%N")
        time=$(echo "$end - $start" | bc -l)
        totaltime=$(echo "$totaltime + $time" | bc -l)

        iters=`echo $res | cut -d ',' -f 1`
        totalit=$((totalit + iters))

        price=`echo $res | cut -d ',' -f 2`
        totalprice=$((totalprice + price))

        if [ $price -gt $best ]
        then
          best=$price
        fi
        #eps=$(echo "( $opt - $price ) / $opt" | bc -l)
        #totaleps=$(echo "$totaleps + $eps" | bc -l)

        echo "${iters},${price},${time}" >> $outfile
      done

      echo "${file},$(echo "$totalit / $RUNS" | bc -l),$(echo "$totalprice / $RUNS" | bc -l),${best},$(echo "$totaltime / $RUNS" | bc -l)" >> $MAINOUTFILE

    done
  done
done
