RUNS=10


for type in data/*/
  do
  if [ ! -d "ilp_${type}" ]
  then
    mkdir "ilp_${type}"
  fi
  for folder in ${type}*/
  do

    if [ -d "ilp_${folder}" ]
    then
      echo "Skipping $folder"
      continue
    fi
    echo "Starting $folder"
    mkdir "ilp_${folder}"

    MAINOUTFILE="ilp_${folder}/main.csv"

    echo "file,opt" > $MAINOUTFILE

    for file in ${folder}*
    do
      #echo "$file"
        ./SA -i "$file" | tail -1 >> $MAINOUTFILE
    done
  done
done
