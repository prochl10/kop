//
// Created by lucie on 23.12.2021.
//
#ifndef KOP_05_SAT_H

#define ll long long
#define ld long double
#include <bits/stdc++.h>

#define KOP_05_SAT_H

using namespace std;

struct Clause{
    Clause(const set<int> &literals);

    bool operator<(const Clause &rhs) const;

    bool operator>(const Clause &rhs) const;

    bool operator<=(const Clause &rhs) const;

    bool operator>=(const Clause &rhs) const;

    set<int> variables;

    bool isSat(vector<bool> & x) const;
};


class SAT {
    ll equilibriumBound = 200;
    set<Clause> clauses;
    int clauses_count;
    int variablesCount;
    vector<int> weights;
public:
    void setEquilibriumBound(long long int equilibriumBound);

private:
    ll weightsSum = 0;
public:
    const set<Clause> &getClauses() const;

    ll getWeight(int i) const;

    int getSize();

    SAT(int l, int c);

    static bool frozen(ll curr_temp);

    bool equilibrium(ll n) const;

    pair<ll, vector<bool>> update(const pair<ll, vector<bool>> & curr_configuration) const;

    static bool better(ll new_solution, ll old_solution);

    static ld cool(ld curr_temp, ld cooling_factor);

    static vector<ll> random_permutation(long long int size);

    void addClause(int a, int b, int c);

    bool isSat(vector<bool> & x) const;

    void setWeights(vector<int> weights);

    void setWeightsSum(ll weightsSum);

};


#endif //KOP_05_SAT_H
