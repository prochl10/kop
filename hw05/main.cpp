#include <iostream>
#include <bits/getopt_core.h>
#include "SimulatedAnnealingSolver.h"
#include "SAT.h"
#include "gurobi_c++.h"

#define ll long long
#define ld long double

#define DEFAULT_BOUND 250
#define DEFAULT_TEMPERATURE -1
#define DEFAULT_COOLING_FACTOR 0.995
#define DEFAULT_TAKE_RATE 0.01

SAT parseProblem(const string &filename) {
    ifstream file;
    file.open(filename);
    string line;
    if (file.is_open()) {

        //skip comments
        while (getline(file, line) && line[0] == 'c');
        int literals, clauses;

        assert(sscanf(line.c_str(), "p mwcnf %d %d", &literals, &clauses) == 2);

        SAT sat(literals, clauses);
        //skip comments
        while (getline(file, line) && line[0] == 'c');

        vector<int> weights;
        char _;
        int tmp;
        stringstream w(line);

        w >> _;
        ll sum = 0;

        weights.push_back(42);
        //read weights
        for (auto i = 0; i < literals; ++i) {
            w >> tmp;
            sum += tmp;
            weights.push_back(tmp);
        }
        sat.setWeightsSum(sum);
        sat.setWeights(weights);
        //skip comments
        while (getline(file, line) && line[0] == 'c');
        //read clauses
        int a, b, c;
        do {
            stringstream str(line);
            str >> a >> b >> c;
            sat.addClause(a, b, c);
        } while (getline(file, line) && line[0] != '%');

        return sat;
    } else {
        cerr << "Could not open file " << filename << endl;
        abort();
    }
}

pair<ll,vector<bool>> ilp(SAT &sat) {
    try {
        GRBEnv env("gurobi.log");

        env.start();

        GRBModel model = GRBModel(env);

        vector<GRBVar> vars;
        vars.reserve(sat.getSize() + 1);

        for (auto i = 0; i < sat.getSize(); ++i) {
            vars.push_back(model.addVar(0.0, 1.0, sat.getWeight(i+1), GRB_BINARY));
        }
        model.set(GRB_IntAttr_ModelSense, GRB_MAXIMIZE);
        for (auto &c: sat.getClauses()) {
            GRBLinExpr expr;

            for (auto l: c.variables) {
                expr += l > 0 ? vars[l-1] : 1 - vars[-l-1];
            }

            model.addConstr(expr >= 1);
        }

        model.set(GRB_IntParam_LogToConsole, 0);
        model.optimize();

        int res = model.get(GRB_DoubleAttr_ObjVal);
        vector<bool> conf;
        conf.push_back(false);
        for(auto & v : vars){
            conf.push_back(v.get(GRB_DoubleAttr_X));
        }
        return {res,conf};
    }
    catch (GRBException & e) {
        cout << " Error code = " << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;
    } catch (...) {
        cout << " Exception during optimization " << endl;
    }
    return {INT64_MIN, {}};
}


int main(int argc, char *argv[]) {

    optind = 1;
    int c;
    int optarg_int;
    double optarg_double;
    ll bound = DEFAULT_BOUND, start_temperature = DEFAULT_TEMPERATURE;
    ld cooling_factor = DEFAULT_COOLING_FACTOR, take_rate = DEFAULT_TAKE_RATE;
    srand(static_cast<unsigned int>(std::chrono::steady_clock::now().time_since_epoch().count()));
    bool ilp_flag = false;

    while (~(c = getopt(argc, argv, "b:t:c:r:i"))) {
        switch (c) {
            case 'b':
                if (!sscanf(optarg, "%d", &optarg_int)) {
                    cerr << "Could not parse equilibrium bound" << endl;
                    exit(0);
                }
                bound = optarg_int;
                break;
            case 't':
                if (!sscanf(optarg, "%d", &optarg_int)) {
                    cerr << "Could not start temperature" << endl;
                    exit(0);
                }
                if (optarg_int != 0)
                    start_temperature = optarg_int;
                break;
            case 'c':
                if (!sscanf(optarg, "%lf", &optarg_double)) {
                    cerr << "Could not parse cooling factor" << endl;
                    exit(0);
                }
                cooling_factor = optarg_double;
                break;
            case 'r':
                if (!sscanf(optarg, "%lf", &optarg_double)) {
                    cerr << "Could not parse take rate" << endl;
                    exit(0);
                }
                take_rate = optarg_double;
                break;
            case 'i':
                ilp_flag = true;
                break;
            default:
                abort();
        }
    }

    SAT sat = parseProblem(argv[optind]);

    if(ilp_flag)
    {
        auto res = ilp(sat);
        cout << argv[optind]  << "," << res.first << endl;
        return 0;
    }

    sat.setEquilibriumBound(bound);
    SimulatedAnnealingSolver<SAT> solver(
            cooling_factor,
            take_rate,
            sat
    );

    if (start_temperature != -1)
        solver.setStartTemperature(start_temperature);

    solver.optimize(true);

    return 0;
}
