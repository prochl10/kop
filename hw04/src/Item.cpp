//
// Created by lucie on 10.11.2021.
//

#include "Item.h"

Item::Item(ll w, ll p) : weight(w), price(p) {
    rank = p / (ld)w;
}

bool Item::operator<(const Item &rhs) const {
    return rank < rhs.rank;
}

bool Item::operator>(const Item &rhs) const {
    return rhs < *this;
}

bool Item::operator<=(const Item &rhs) const {
    return !(rhs < *this);
}

bool Item::operator>=(const Item &rhs) const {
    return !(*this < rhs);
}
