//
// Created by lucie on 09.11.2021.
//

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <chrono>
#include "Knapsack.h"
#include "SimulatedAnnealingSolver.h"

#pragma GCC optimize("Ofast")
#define ll long long



using namespace std;
using namespace std::chrono;


enum METHOD {
    BNB, DYNPROG, GREEDY, REDUX, FPTAS
};

map<METHOD, string> METHOD_PRINT = {{BNB,     "BNB"},
                                    {DYNPROG, "DYNPROG"},
                                    {REDUX,   "REDUX"},
                                    {GREEDY,  "GREEDY"},
                                    {FPTAS,   "FPTAS"}};

// Tested  instances.
vector<int> INSTANCES = {4, 10, 15, 20, 22, 25, 27, 30}; //, 32, 35}; // 37, 40};
vector<METHOD> METHODS = {BNB, DYNPROG, REDUX};
string PROBLEM_POSTFIX = "_inst.dat";
string SOLUTION_POSTFIX = "_sol.dat";
vector<double> ERRORS = {0.25, 0.5, 0.75};
vector<string> PREFIXES = {"ZKW", "NK", "ZKC"};

Knapsack parseProblem(string &problem) {
    stringstream str(problem);
    ll id, n, capacity, max_price = 0, w, p;
    vector<Item> items;
    str >> id >> n >> capacity;
    for (ll i = 0; i < n; ++i) {
        str >> w >> p;
        if (w <= capacity) {
            items.emplace_back(w, p);
            max_price += p;
        }
    }
    return {n, id, capacity, max_price, items, 10000};
}

map<int, Knapsack> readFile(string &filename) {
    ifstream file;
    file.open(filename);
    string line;
    map<int, Knapsack> vec;

    if (file.is_open()) {
        while (getline(file, line)) {
            auto p = parseProblem(line);
            vec[p.getId()] = p;
        }
        file.close();
    }
    return vec;
}

void parseSol(string &line, map<int, Knapsack> &problems) {
    stringstream str(line);
    int id, _;
    ll price;
    str >> id >> _ >> price;
    problems.at(id).setSolution(price);
}

void readSols(const string &filename, map<int, Knapsack> &problems) {
    ifstream file;
    file.open(filename);
    string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            parseSol(line, problems);
        }
        file.close();
    }
}

int main(int argc, char *argv[]) {
    ofstream out;
    out.open("out_configurations_fine.csv");
    ofstream mistakes;
    out << "mismatched,bound,startTemperature,coolingFactor,takeRate,maxEps,meanEps,maxIter,meanIter" << endl;

    auto start = high_resolution_clock::now();
    auto stop = high_resolution_clock::now();
    auto dur = duration_cast<microseconds>(stop - start);
    string problem_file_name = argv[1];
    auto problems = readFile(problem_file_name);
    //readSols(argv[2], problems);
    auto bound = 200;
    auto rate = 1;
    auto c_factor = 995;



    ld max_eps = -1, mean_eps = 0;
    ll max_iter = -1, mean_iter = 0;
    auto counter = 0;
    for (auto problem: problems) {
        problem.second.setEquilibriumBound(bound);
        problem.second.setSolution(problem.second.dynProg());
        SimulatedAnnealingSolver<Knapsack> solver(
                static_cast<double>(c_factor) / 1000,
                static_cast<double>(rate) / 100,
                problem.second
        );
        auto res = solver.optimize(true);

        if (max_iter < solver.getIterations())
            max_iter = solver.getIterations();
        mean_iter += solver.getIterations();

        if (res.first != problem.second.getSolution()) {
            auto eps = static_cast<ld>(abs(res.first - problem.second.getSolution()))
                       / static_cast<ld>(max(res.first, problem.second.getSolution()));
            if (max_eps < eps)
                max_eps = eps;
            mean_eps += eps;
            ++counter;
        }
    }
    /*out << counter << "," << bound << "," << temp << "," << c_factor << "," << rate << "," << max_eps
        << ","
        << static_cast<ld>(mean_eps) / problems.size() << "," << max_iter << ","
        << static_cast<ld>(mean_iter) / problems.size() << endl;*/



    return 0;

}

