//
// Created by lucie on 09.11.2021.
//

#include <string>
#include <algorithm>
#include <cmath>
#include <iostream>
#include "Knapsack.h"
#include <climits>
#include <tuple>
#include <functional>

ll Knapsack::getSize() const {
    return size;
}

void Knapsack::setSize(ll size) {
    Knapsack::size = size;
}

ll Knapsack::getId() const {
    return id;
}

void Knapsack::setId(ll id) {
    Knapsack::id = id;
}

ll Knapsack::getCapacity() const {
    return capacity;
}

void Knapsack::setCapacity(ll capacity) {
    Knapsack::capacity = capacity;
}

ll Knapsack::getMaxPrice() const {
    return maxPrice;
}

void Knapsack::setMaxPrice(ll maxPrice) {
    Knapsack::maxPrice = maxPrice;
}

const vector<Item> &Knapsack::getItems() const {
    return items;
}

void Knapsack::setItems(const vector<Item> &items) {
    Knapsack::items = items;
}

ll Knapsack::getSolution() const {
    return solution;
}

void Knapsack::setSolution(ll solution) {
    Knapsack::solution = solution;
}

Knapsack::Knapsack(ll size, ll id, ll capacity, ll maxPrice, const vector<Item> &items, ll equilibrium = 1000) :
        size(size), id(id), capacity(capacity), maxPrice(maxPrice), equilibriumBound(equilibrium),
        items(items) {}

void Knapsack::branchBounds(ll curr_ptr, ll curr_weight, ll curr_price, ll price_to_add, const string &combination) {
    if (curr_ptr == items.size()) {
        if (curr_price > currSolution && curr_weight <= capacity)
            currSolution = curr_price;
        return;
    }
    if (curr_weight > capacity || curr_price + price_to_add < currSolution) {
        return;
    }
    branchBounds(curr_ptr + 1, curr_weight + items[curr_ptr].weight, curr_price + items[curr_ptr].price,
                 price_to_add - items[curr_ptr].price, combination + "1");
    branchBounds(curr_ptr + 1, curr_weight, curr_price, price_to_add - items[curr_ptr].price, combination + "0");
}

ll Knapsack::branchBounds() {
    branchBounds(0, 0, 0, maxPrice);
    return currSolution;
}


ll Knapsack::greedy() {
    vector<Item> v;
    for (auto &i: items)
        v.emplace_back(i);
    sort(v.begin(), v.end());

    greedySolution = 0;
    ll curr_weight = 0;
    for (auto &i: v) {
        if (i.weight + curr_weight <= capacity) {
            curr_weight += i.weight;
            greedySolution += i.price;
        } else
            break;
    }

    return greedySolution;
}

ld Knapsack::greedyEps() {
    if (greedySolution == -1)
        greedy();
    if (max(greedySolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(greedySolution - solution)) / static_cast<ld>(max(greedySolution, solution));
}

ll Knapsack::redux() {
    if (greedySolution == -1)
        greedy();
    maxPrice = -1;
    Item mostExpensive(0, 0);
    for (auto &i: items)
        if (i.price > maxPrice)
            mostExpensive = i;
    if (greedySolution > mostExpensive.price)
        reduxSolution = greedySolution;
    else {
        reduxSolution = mostExpensive.price;
    }
    return reduxSolution;
}

ld Knapsack::reduxEps() {
    if (reduxSolution == -1)
        redux();
    if (max(reduxSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(reduxSolution - solution)) / static_cast<ld>(max(reduxSolution, solution));
}

long long int Knapsack::getGreedySolution() const {
    return greedySolution;
}

long long int Knapsack::getReduxSolution() const {
    return reduxSolution;
}

ll Knapsack::dynProg() {
    if (items.empty())
        return 0;

    auto rows = 1 + maxPrice;
    ll curr_price = 0;
    vector<ll> table(rows * items.size(), LONG_LONG_MAX);

    if (items[0].price != 0) {
        table[items[0].price] = items[0].weight;
    }
    table[0] = 0;

    for (auto column = 1; column < items.size(); column++) {
        auto prevCol = column - 1;
        auto colWeight = items[column].weight;
        auto colPrice = items[column].price;

        for (auto row = 0; row < rows; row++) {

            table[column * rows + row] = table[prevCol * rows + row]; // Duplicate item on the left


            if (items[column].price <= row
                && table[prevCol * rows + row - colPrice] != LONG_LONG_MAX
                && table[prevCol * rows + row - colPrice] + colWeight <
                   table[column * rows + row]
                    ) { // Compare  item on the left bottom
                table[column * rows + row] =
                        table[prevCol * rows + row - colPrice] + colWeight;
            }
        }
    }

    auto lastColumn = items.size() - 1;

    for (ll i = rows - 1; i >= 0; i--) {
        if (capacity >= table[lastColumn * rows + i]) {
            curr_price = i;
            break;
        }
    }

    dynProgSolution = curr_price;
    return curr_price;
}

ld Knapsack::dynProgEps() {
    if (dynProgSolution == -1)
        dynProg();
    if (max(dynProgSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(dynProgSolution - solution)) / static_cast<ld>(max(dynProgSolution, solution));
}


ll Knapsack::fptas(double error) {
    ll maxCost = -1;
    for (auto item: items)
        if (item.price > maxCost)
            maxCost = item.price;
    double k = (static_cast<ld>(error) * static_cast<ld>(maxCost)) / static_cast<ld>(items.size());
    cout << k << endl;
    vector<Item> fptasItems;
    ll mFptasPrice = 0;

    for (auto &item: items) {
        ll tmpPrice = floor(static_cast<ld> (item.price) / k);
        mFptasPrice += tmpPrice;
        fptasItems.emplace_back(Item(item.weight, tmpPrice));
    }

    auto fptasKnapsack = Knapsack(size, id, capacity, mFptasPrice, fptasItems);

    auto itemsFlag = fptasKnapsack.fptasDyn();

    fptasSolution = 0;

    for (auto i = 0; i < itemsFlag.size(); ++i) {
        if (itemsFlag[i]) {
            fptasSolution += items[i].price;
        }
    }
    return fptasSolution;
}

vector<bool> Knapsack::fptasDyn() {
    if (items.empty())
        return {};

    auto rows = 1 + maxPrice;
    ll curr_price = 0;
    vector<ll> table(rows * items.size(), LONG_LONG_MAX);

    if (items[0].price != 0) {
        table[items[0].price] = items[0].weight;
    }
    table[0] = 0;

    for (auto column = 1; column < items.size(); column++) {
        auto prevCol = column - 1;
        auto colWeight = items[column].weight;
        auto colPrice = items[column].price;

        for (auto row = 0; row < rows; row++) {

            table[column * rows + row] = table[prevCol * rows + row]; // Duplicate item on the left


            if (items[column].price <= row
                && table[prevCol * rows + row - colPrice] != LONG_LONG_MAX
                && table[prevCol * rows + row - colPrice] + colWeight <
                   table[column * rows + row]
                    ) { // Compare  item on the left bottom
                table[column * rows + row] =
                        table[prevCol * rows + row - colPrice] + colWeight;
            }
        }
    }

    vector<bool> itemsFlag;
    for (auto &item: items) {
        itemsFlag.push_back(false);
    }

    auto lastColumn = items.size() - 1;

    for (ll i = rows - 1; i >= 0; i--) {
        if (capacity >= table[lastColumn * rows + i]) {
            curr_price = i;
            break;
        }
    }

    ll tmp = curr_price;

    for (auto i = items.size() - 1; i >= 0; i--) {
        if (i != 0
            && table[(i - 1) * rows + tmp] != table[i * rows + tmp]
                ) {
            tmp -= items[i].price;
            itemsFlag[i] = true;
        }

        if (i == 0) {
            if (tmp != 0) {
                itemsFlag[0] = true;
            }
            break;
        }
    }

    return itemsFlag;
}

ld Knapsack::fptasEps(double error) {
    if (fptasSolution == -1)
        fptas(error);
    if (max(fptasSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(fptasSolution - solution)) / static_cast<ld>(max(fptasSolution, solution));
}

Knapsack::Knapsack() {}

void Knapsack::setFptasSolution(long long int fptasSolution) {
    Knapsack::fptasSolution = fptasSolution;
}

bool Knapsack::frozen(long long int curr_temp) {
    return curr_temp < 1;
}

bool Knapsack::better(long long int new_solution, long long int old_solution) {
    return new_solution > old_solution;
}

ld Knapsack::cool(long double curr_temp, long double cooling_factor) {
    return curr_temp * cooling_factor;
}

pair<ll, vector<bool>> Knapsack::update(const pair<long long int, const vector<bool>> &curr_configuration) const {
    auto gen = bind(uniform_int_distribution<>(0, 1), default_random_engine()); // NOLINT(modernize-avoid-bind)

    ll weight = 0;
    auto price = curr_configuration.first;
    auto res = curr_configuration.second;
    for (auto i = 0; i < items.size(); ++i) {
        if (res[i]) {
            res[i] = gen();
            if(!res[i])
                price -= items[i].price;
            else
                weight += items[i].weight;
        }
    }
    auto perm = random_permutation(items.size());

    for(auto i : perm)
    {
        if(!res[i] && weight + items[i].weight <= capacity)
        {
            res[i] = true;
            weight += items[i].weight;
            price += items[i].price;

            if(weight == capacity)
                break;
        }
    }

    return {price,res};
}

bool Knapsack::equilibrium(long long int n) const {
    return n >= equilibriumBound;
}

vector<ll> Knapsack::random_permutation(long long int size) {
    vector<ll> v(size);

    for(ll i = 0; i < size; ++i )
        v[i] = i;

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    return v;
}

void Knapsack::setEquilibriumBound(long long int equilibrium_bound) {
    Knapsack::equilibriumBound = equilibrium_bound;
}

