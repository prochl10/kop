//
// Created by lucie on 13.12.2021.
//
#include <cstdlib>

#ifndef KOP_HW01_SIMULATEDANNEALINGSOLVER_H

#include <vector>
#include <cmath>

#define ll long long
#define ld long double

#define KOP_HW01_SIMULATEDANNEALINGSOLVER_H
using namespace std;

template<typename Problem>
class SimulatedAnnealingSolver {

private:
    ld coolingFactor;
    ld takeRate;
    Problem problem;
    pair<ll, vector<bool>> bestSolution;
    ll iterations = -1;
    ld startTemperature{};

public:
    long long int getIterations() const {
        return iterations;
    }

public:
    pair<long long int, vector<bool>> optimize(bool out = false) {
        auto curr_temp = startTemperature;
        pair<long long int, vector<bool>> curr_solution = {0, bestSolution.second};
        ll total_iter = 0;
        ofstream outFile;
        if (out) {
            string filename = "out_graph_bound200.csv";
            outFile.open(filename);
            outFile << "iter,eps,bestPrice,currPrice" << endl;
        }

        while (!problem.frozen(curr_temp)) {
            ll iter = 0;
            ll taken_ctr = 0;
            pair<ll, vector<bool>> new_solution;
            while (!problem.equilibrium(iter)) {
                new_solution = problem.update(curr_solution);
                if (problem.better(new_solution.first, curr_solution.first)) {
                    if (problem.better(new_solution.first, bestSolution.first))
                        bestSolution = new_solution;
                    curr_solution.swap(new_solution);
                    ++taken_ctr;
                } else {
                    auto delta = curr_solution.first - new_solution.first > 0 ?
                                 curr_solution.first - new_solution.first :
                                 new_solution.first - curr_solution.first;
                    if (drand48() < exp(-delta / curr_temp)) {
                        ++taken_ctr;
                        curr_solution.swap(new_solution);
                    }
                }
                if (out) {
                    auto eps = static_cast<ld>(abs(curr_solution.first - problem.getSolution()))
                               / static_cast<ld>(max(curr_solution.first, problem.getSolution()));
                    outFile << total_iter << "," << eps << "," << bestSolution.first << "," << curr_solution.first
                            << endl;
                }
                ++iter;
                ++total_iter;
            }
            //cerr << static_cast<ld>(taken_ctr) << "\t" << static_cast<ld>(iter) << "\t" << takeRate << endl;

            if (static_cast<ld>(taken_ctr) / static_cast<ld>(iter) < takeRate)
                break;

            curr_temp = problem.cool(curr_temp, coolingFactor);
        }
        //cerr << total_iter << " " << bestSolution.first << endl;
        iterations = total_iter;
        return bestSolution;
    };

    ld setTemperature() {
        auto curr = bestSolution;
        auto new_sol = bestSolution;
        vector<ll> bad;

        for(auto i = 0; i <= 100; ++i) {
            new_sol = problem.update(curr);
            if(!problem.better(new_sol.first, curr.first)){
                bad.push_back(curr.first - new_sol.first);
            }
            curr.swap(new_sol);
        }
        startTemperature = static_cast<ld>(accumulate(bad.begin(), bad.end(), 0))/bad.size();

        return startTemperature;
    }

    SimulatedAnnealingSolver(long double start_temperature,
                             long double cooling_factor,
                             long double take_rate,
                             Problem problem) : coolingFactor(cooling_factor),
                                                takeRate(take_rate),
                                                problem(move(problem)),
                                                bestSolution({-1, vector<bool>(problem.getSize(), false)}),
                                                startTemperature(start_temperature){};

    SimulatedAnnealingSolver(long double cooling_factor,
                             long double take_rate,
                             Problem problem): coolingFactor(cooling_factor),
                                               takeRate(take_rate),
                                               problem(move(problem)),
                                               bestSolution({-1, vector<bool>(problem.getSize(), false)}),
                                               startTemperature(setTemperature()){}

    long double getStartTemperature() const;;

};

template<typename Problem>
long double SimulatedAnnealingSolver<Problem>::getStartTemperature() const {
    return startTemperature;
}


#endif //KOP_HW01_SIMULATEDANNEALINGSOLVER_H
