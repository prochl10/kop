//
// Created by lucie on 09.11.2021.
//



#ifndef KOP_HW01_OPTIMIZEKNAPSACK_H
#pragma  once
#include <vector>
#include "Item.h"
#include <random>
#include <iostream>


#define ll long long
#define KOP_HW01_OPTIMIZEKNAPSACK_H

using namespace std;

class Knapsack {
public:
    void setFptasSolution(long long int fptasSolution);

    Knapsack();

    long long int getGreedySolution() const;

    long long int getReduxSolution() const;

private:
    ll size, id,  capacity,  maxPrice, greedySolution = -1,
    reduxSolution = -1, solution = -1, currSolution  = -1, dynProgSolution = -1, fptasSolution = -1;
    ll equilibriumBound = 1000;
public:
    void setEquilibriumBound(long long int equilibrium_bound);

private:
    vector<Item> items;
public:
    Knapsack(ll size, ll id, ll capacity, ll maxPrice,
             const vector<Item> &items, ll equilibrium);

    ll getCapacity() const;

    void setCapacity(ll capacity);

    ll getMaxPrice() const;

    void setMaxPrice(ll maxPrice);

    const vector<Item> &getItems() const;

    void setItems(const vector<Item> &items);

    ll getSolution() const;

    void setSolution(ll solution);

    ll getId() const;

    void setId(ll id);

    ll getSize() const;

    void setSize(ll size);

    void branchBounds(ll curr_ptr, ll curr_weight, ll curr_price, ll price_to_add, const string& combination = "");

    ll branchBounds();

    ll greedy();

    ld greedyEps();

    ll redux();

    ld reduxEps();

    ll dynProg();

    ld dynProgEps();

    ll fptas(double error);

    ld fptasEps(double error);

    vector<bool> fptasDyn();

    pair<vector<long long int>, long long int> dynamic_cost(long long int c, long long int n);

    pair<vector<long long int>, long long int> dynamic_cost(long long int c, unsigned long n);

    static bool frozen(ll curr_temp);

    bool equilibrium(ll n) const;

    pair<ll, vector<bool>> update(const pair<ll, const vector<bool>> & curr_configuration) const;

    static bool better(ll new_solution, ll old_solution);

    static ld cool(ld curr_temp, ld cooling_factor);

    static vector<ll> random_permutation(long long int size);
};


#endif //KOP_HW01_OPTIMIZEKNAPSACK_H
