//
// Created by lucie on 09.11.2021.
//

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <chrono>
#include "Knapsack.h"


#define ll long long
using namespace std;
using namespace std::chrono;


enum METHOD {
    BNB, DYNPROG, GREEDY, REDUX, FPTAS
};

vector<string> METHOD_PRINT = {"BNB", "DYNPROG", "GREEDY", "REDUX", "FPTAS"};

// Tested  instances.
vector<int> INSTANCES = {4, 10, 15, 20, 22, 25, 27, 30}; //, 32, 35}; // 37, 40};
vector<METHOD> METHODS = { BNB, DYNPROG, GREEDY, REDUX, FPTAS};
string PROBLEM_POSTFIX = "_inst.dat";
string SOLUTION_POSTFIX = "_sol.dat";
vector<double> ERRORS = {0.25, 0.5, 0.75};
vector<string> PREFIXES = {"ZKW", "NK", "ZKC"};

Knapsack parseProblem(string &problem) {
    stringstream str(problem);
    ll id, n, capacity, max_price = 0, w, p;
    vector<Item> items;
    str >> id >> n >> capacity;
    for (ll i = 0; i < n; ++i) {
        str >> w >> p;
        if (w <= capacity) {
            items.emplace_back(w, p);
            max_price += p;
        }
    }
    return {n, id, capacity, max_price, items};
}

map<int,Knapsack> readFile(string &filename) {
    ifstream file;
    file.open(filename);
    string line;
    map<int,Knapsack> vec;

    if (file.is_open()) {
        while (getline(file, line)) {
            auto p =  parseProblem(line);
            vec[p.getId()] = p;
        }
        file.close();
    }
    return vec;
}

void parseSol(string &line, map<int,Knapsack> &problems) {
    stringstream str(line);
    int id, _;
    ll price;
    str >> id >> _ >> price;
    problems.at(id).setSolution(price);
}

void readSols(string &filename, map<int,Knapsack> &problems) {
    ifstream file;
    file.open(filename);
    string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            parseSol(line, problems);
        }
        file.close();
    }
}

int main() {
    ofstream out;
    out.open("outbnb_bad.csv");
    ofstream mistakes;
    mistakes.open("mistakes_bad.csv");
    out << "id,n,type,method,time,eps,fptas_error" << endl;
    mistakes << "method,n,type,max,mean,err" << endl;
    cout << "Instances to run are";
    for (auto i: INSTANCES) {
        cout << " " << i;
    }
    cout << endl;
    auto start = high_resolution_clock::now();
    auto stop = high_resolution_clock::now();
    auto dur = duration_cast<microseconds>(stop - start);


    for (auto prefix :PREFIXES ) {
        cout << "Tested type is " << prefix << endl;
        for (auto method: METHODS) {
            cout << "Tested method " << METHOD_PRINT[method] << endl;
            for (auto i: INSTANCES) {
                cout << "Current size is " << i << endl;

                auto problem_file_name = "data/" + prefix + to_string(i) + PROBLEM_POSTFIX;
                auto solution_file_name = "data/" + prefix + to_string(i) + SOLUTION_POSTFIX;

                auto problems = readFile(problem_file_name);
                readSols(solution_file_name, problems);
                ld maxEps = -1, meanEps = 0;
                switch (method) {
                    case BNB: {
                        cout <<"BNB" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.branchBounds();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            if (s != p.second.getSolution())
                                cout << "Ooopsie " << p.second.getId() << endl;
                           out << p.second.getId() << "," << i << "," << prefix << "," << METHOD_PRINT[method] << ","
                              << dur.count() << "," << 0 << endl;
                        }
                        break;
                    }
                    case GREEDY:

                        maxEps = -1;
                        meanEps = 0;
                        cout <<"GREEd" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.greedyEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            if (s > maxEps)
                                maxEps = s;
                            out << p.second.getId() << "," << i << "," << prefix << "," << METHOD_PRINT[method] << ","
                                 << dur.count() << "," << s << endl;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        cout << endl << "Greedy: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        mistakes << METHOD_PRINT[method] << "," << i << "," << prefix << "," << maxEps << "," << meanEps << endl;
                        break;
                    case REDUX:
                        maxEps = -1;
                        meanEps = 0;
                        cout <<"RED" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.reduxEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            if (s > maxEps)
                                maxEps = s;
                            out << p.second.getId() << "," << i << "," << prefix << "," << METHOD_PRINT[method] << ","
                                << dur.count() << "," << s << endl;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        cout << endl << "Redux: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        mistakes << METHOD_PRINT[method] << "," << i << "," << prefix << "," << maxEps << "," << meanEps << endl;

                        break;
                    case DYNPROG:
                        maxEps = -1;
                        meanEps = 0;
                        cout <<"DYN" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            auto s = p.second.dynProgEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            out << p.second.getId() << "," << i << "," << prefix << "," << METHOD_PRINT[method] << ","
                                << dur.count() << "," << s << endl;
                            if (s > maxEps)
                                maxEps = s;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        mistakes << METHOD_PRINT[method] << "," << i << "," << prefix << "," << maxEps << "," << meanEps <<  endl;

                        break;

                    case FPTAS:
                        cout <<"FPTAS" << METHOD_PRINT[method] << endl;
                        for (auto e: ERRORS) {
                            maxEps = -1;
                            meanEps = 0;
                            for (auto &p: problems) {
                                if(p.first % 100 == 0)
                                    cout << p.first;
                                p.second.setFptasSolution(-1);
                                auto s = p.second.fptasEps(e);

                                stop = high_resolution_clock::now();
                                dur = duration_cast<microseconds>(stop - start);
                                out << p.second.getId() << "," << i << "," << prefix << "," << METHOD_PRINT[method]
                                    << ","
                                    << dur.count() << "," << s << endl;
                                if (s > maxEps)
                                    maxEps = s;
                                meanEps += s;
                            }
                            meanEps /= problems.size();
                            mistakes << METHOD_PRINT[method] << "," << i << "," << prefix << "," << maxEps << "," << meanEps << "," << e <<  endl;
                            cout << endl << "FPTAS: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        }

                        break;
                    default:
                        cout << "Method not implemented";
                }

            }
        }

    }
    return  0;
}

