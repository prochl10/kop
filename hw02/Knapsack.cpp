//
// Created by lucie on 09.11.2021.
//

#include <string>
#include <algorithm>
#include <cmath>
#include <iostream>
#include "Knapsack.h"
#include <climits>
#include <tuple>

ll Knapsack::getSize() const {
    return size;
}

void Knapsack::setSize(ll size) {
    Knapsack::size = size;
}

ll Knapsack::getId() const {
    return id;
}

void Knapsack::setId(ll id) {
    Knapsack::id = id;
}

ll Knapsack::getCapacity() const {
    return capacity;
}

void Knapsack::setCapacity(ll capacity) {
    Knapsack::capacity = capacity;
}

ll Knapsack::getMaxPrice() const {
    return maxPrice;
}

void Knapsack::setMaxPrice(ll maxPrice) {
    Knapsack::maxPrice = maxPrice;
}

const vector<Item> &Knapsack::getItems() const {
    return items;
}

void Knapsack::setItems(const vector<Item> &items) {
    Knapsack::items = items;
}

ll Knapsack::getSolution() const {
    return solution;
}

void Knapsack::setSolution(ll solution) {
    Knapsack::solution = solution;
}

Knapsack::Knapsack(ll size, ll id, ll capacity, ll maxPrice, const vector<Item> &items) :
        size(size), id(id), capacity(capacity), maxPrice(maxPrice),
        items(items) {}

void Knapsack::branchBounds(ll curr_ptr, ll curr_weight, ll curr_price, ll price_to_add, const string &combination) {
    if (curr_ptr == items.size()) {
        if (curr_price > currSolution && curr_weight <= capacity)
            currSolution = curr_price;
        return;
    }
    if (curr_weight > capacity || curr_price + price_to_add < currSolution) {
        return;
    }
    branchBounds(curr_ptr + 1, curr_weight + items[curr_ptr].w, curr_price + items[curr_ptr].p,
                 price_to_add - items[curr_ptr].p, combination + "1");
    branchBounds(curr_ptr + 1, curr_weight, curr_price, price_to_add - items[curr_ptr].p, combination + "0");
}

ll Knapsack::branchBounds() {
    branchBounds(0, 0, 0, maxPrice);
    return currSolution;
}


ll Knapsack::greedy() {
    vector<Item> v;
    for (auto &i: items)
        v.emplace_back(i);
    sort(v.begin(), v.end());

    greedySolution = 0;
    ll curr_weight = 0;
    for (auto &i: v) {
        if (i.w + curr_weight <= capacity) {
            curr_weight += i.w;
            greedySolution += i.p;
        } else
            break;
    }

    return greedySolution;
}

ld Knapsack::greedyEps() {
    if (greedySolution == -1)
        greedy();
    if (max(greedySolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(greedySolution - solution)) / static_cast<ld>(max(greedySolution, solution));
}

ll Knapsack::redux() {
    if (greedySolution == -1)
        greedy();
    maxPrice = -1;
    Item mostExpensive(0, 0);
    for (auto &i: items)
        if (i.p > maxPrice)
            mostExpensive = i;
    if (greedySolution > mostExpensive.p)
        reduxSolution = greedySolution;
    else {
        reduxSolution = mostExpensive.p;
    }
    return reduxSolution;
}

ld Knapsack::reduxEps() {
    if (reduxSolution == -1)
        redux();
    if (max(reduxSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(reduxSolution - solution)) / static_cast<ld>(max(reduxSolution, solution));
}

long long int Knapsack::getGreedySolution() const {
    return greedySolution;
}

long long int Knapsack::getReduxSolution() const {
    return reduxSolution;
}

ll Knapsack::dynProg() {
    if (items.empty())
        return 0;

    auto rows = 1 + maxPrice;
    ll curr_price = 0;
    vector<ll> table(rows * items.size(), LONG_LONG_MAX);

    if (items[0].p != 0) {
        table[items[0].p] = items[0].w;
    }
    table[0] = 0;

    for (auto column = 1; column < items.size(); column++) {
        auto prevCol = column - 1;
        auto colWeight = items[column].w;
        auto colPrice = items[column].p;

        for (auto row = 0; row < rows; row++) {

            table[column * rows + row] = table[prevCol * rows + row]; // Duplicate item on the left


            if (items[column].p <= row
                && table[prevCol * rows + row - colPrice] != LONG_LONG_MAX
                && table[prevCol * rows + row - colPrice] + colWeight <
                   table[column * rows + row]
                    ) { // Compare  item on the left bottom
                table[column * rows + row] =
                        table[prevCol * rows + row - colPrice] + colWeight;
            }
        }
    }

    auto lastColumn = items.size() - 1;

    for (ll i = rows - 1; i >= 0; i--) {
        if (capacity >= table[lastColumn * rows + i]) {
            curr_price = i;
            break;
        }
    }

    dynProgSolution = curr_price;
    return curr_price;
}

ld Knapsack::dynProgEps() {
    if (dynProgSolution == -1)
        dynProg();
    if (max(dynProgSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(dynProgSolution - solution)) / static_cast<ld>(max(dynProgSolution, solution));
}


ll Knapsack::fptas(double error) {
    ll maxCost = -1;
    for (auto item: items)
        if (item.p > maxCost)
            maxCost = item.p;
    double k = (static_cast<ld>(error) * static_cast<ld>(maxCost)) / static_cast<ld>(items.size());
    cout << k  << endl;
    vector<Item> fptasItems;
    ll mFptasPrice = 0;

    for (auto &item: items) {
        ll tmpPrice = floor(static_cast<ld> (item.p) / k);
        mFptasPrice += tmpPrice;
        fptasItems.emplace_back(Item(item.w, tmpPrice));
    }

    auto fptasKnapsack = Knapsack(size, id, capacity, mFptasPrice, fptasItems);

    auto itemsFlag = fptasKnapsack.fptasDyn();

    fptasSolution = 0;

    for (auto i = 0; i < itemsFlag.size(); ++i) {
        if (itemsFlag[i]) {
            fptasSolution += items[i].p;
        }
    }
    return fptasSolution;
}

vector<bool> Knapsack::fptasDyn() {
    if (items.empty())
        return {};

    auto rows = 1 + maxPrice;
    ll curr_price = 0;
    vector<ll> table(rows * items.size(), LONG_LONG_MAX);

    if (items[0].p != 0) {
        table[items[0].p] = items[0].w;
    }
    table[0] = 0;

    for (auto column = 1; column < items.size(); column++) {
        auto prevCol = column - 1;
        auto colWeight = items[column].w;
        auto colPrice = items[column].p;

        for (auto row = 0; row < rows; row++) {

            table[column * rows + row] = table[prevCol * rows + row]; // Duplicate item on the left


            if (items[column].p <= row
                && table[prevCol * rows + row - colPrice] != LONG_LONG_MAX
                && table[prevCol * rows + row - colPrice] + colWeight <
                   table[column * rows + row]
                    ) { // Compare  item on the left bottom
                table[column * rows + row] =
                        table[prevCol * rows + row - colPrice] + colWeight;
            }
        }
    }

    vector<bool> itemsFlag;
    for (auto &item: items) {
        itemsFlag.push_back(false);
    }

    auto lastColumn = items.size() - 1;

    for (ll i = rows - 1; i >= 0; i--) {
        if (capacity >= table[lastColumn * rows + i]) {
            curr_price = i;
            break;
        }
    }

    ll tmp = curr_price;

    for (auto i = items.size() - 1; i >= 0; i--) {
        if (i != 0
            && table[(i - 1) * rows + tmp] != table[i * rows + tmp]
                ) {
            tmp -= items[i].p;
            itemsFlag[i] = true;
        }

        if (i == 0) {
            if (tmp != 0) {
                itemsFlag[0] = true;
            }
            break;
        }
    }

    return itemsFlag;
}

ld Knapsack::fptasEps(double error) {
    if (fptasSolution == -1)
        fptas(error);
    if (max(fptasSolution, solution) == 0)
        return 0;
    return static_cast<ld>(abs(fptasSolution - solution)) / static_cast<ld>(max(fptasSolution, solution));
}

Knapsack::Knapsack() {}

void Knapsack::setFptasSolution(long long int fptasSolution) {
    Knapsack::fptasSolution = fptasSolution;
}

