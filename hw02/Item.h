//
// Created by lucie on 10.11.2021.
//


#ifndef KOP_HW01_ITEM_H

#pragma once
#define ll long long
#define  ld long double
#define KOP_HW01_ITEM_H

using namespace std;

struct Item {
    ll w, p;
    ld rank;

    Item(ll w, ll p);

    bool operator<(const Item &rhs) const;

    bool operator>(const Item &rhs) const;

    bool operator<=(const Item &rhs) const;

    bool operator>=(const Item &rhs) const;
};


#endif //KOP_HW01_ITEM_H
