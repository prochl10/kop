//
// Created by lucie on 09.11.2021.
//

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <chrono>
#include "Knapsack.h"


#define ll long long
using namespace std;
using namespace std::chrono;


enum METHOD {
    BNB, DYNPROG, GREEDY, REDUX, FPTAS
};

map<METHOD, string> METHOD_PRINT = {{BNB, "BNB"}, {DYNPROG, "DYNPROG"}, {REDUX, "REDUX"}, {GREEDY, "GREEDY"}, {FPTAS, "FPTAS"}};

// Tested  instances.
vector<int> INSTANCES = {4, 10, 15, 20, 22, 25, 27, 30}; //, 32, 35}; // 37, 40};
vector<METHOD> METHODS = { BNB, DYNPROG,  REDUX};
string PROBLEM_POSTFIX = "_inst.dat";
string SOLUTION_POSTFIX = "_sol.dat";
vector<double> ERRORS = {0.25, 0.5, 0.75};
vector<string> PREFIXES = {"ZKW", "NK", "ZKC"};

Knapsack parseProblem(string &problem) {
    stringstream str(problem);
    ll id, n, capacity, max_price = 0, w, p;
    vector<Item> items;
    str >> id >> n >> capacity;
    for (ll i = 0; i < n; ++i) {
        str >> w >> p;
        if (w <= capacity) {
            items.emplace_back(w, p);
            max_price += p;
        }
    }
    return {n, id, capacity, max_price, items};
}

map<int,Knapsack> readFile(string &filename) {
    ifstream file;
    file.open(filename);
    string line;
    map<int,Knapsack> vec;

    if (file.is_open()) {
        while (getline(file, line)) {
            auto p =  parseProblem(line);
            vec[p.getId()] = p;
        }
        file.close();
    }
    return vec;
}

void parseSol(string &line, map<int,Knapsack> &problems) {
    stringstream str(line);
    int id, _;
    ll price;
    str >> id >> _ >> price;
    problems.at(id).setSolution(price);
}

void readSols(string &filename, map<int,Knapsack> &problems) {
    ifstream file;
    file.open(filename);
    string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            parseSol(line, problems);
        }
        file.close();
    }
}

int main() {
    ofstream out;
    out.open("out_03.csv");
    ofstream mistakes;
    mistakes.open("mistakes_hw03.csv");
    out << "id,criteria,value,method,time,eps" << endl;
    mistakes << "method,criteria,value,type,max,mean" << endl;

    /*
    cout << "Instances to run are";
    for (auto i: INSTANCES) {
        cout << " " << i;
    }
    cout << endl;

    */

    auto start = high_resolution_clock::now();
    auto stop = high_resolution_clock::now();
    auto dur = duration_cast<microseconds>(stop - start);

    map<string, pair<string,string>> prefixes = {
            {"m04_inst.dat", { "0.4", "ratio" }},
            {"m02_inst.dat", {"0.2", "ratio"}},
            {"w10000_inst.dat", {"10000", "maxweight"}},
            {"w5000_inst.dat", {"5000", "maxweight"}},
            {"w1000_inst.dat", {"1000", "maxweight"}},
            {"c500_inst.dat", {"500", "maxprice"}},
            {"c10000_inst.dat", {"10000", "maxprice"}},
            {"c5000_inst.dat", {"5000", "maxprice"}},
            {"c1000_inst.dat", {"1000", "maxprice"}},
            {"grm5h_inst.dat", {"-5", "grh"}},
            {"grm5l_inst.dat", {"-5", "grl"}},
            {"grm3l_inst.dat", {"-3", "grl"}},
            {"grm3h_inst.dat", {"-3", "grh"}},
            {"grm1h_inst.dat", {"-1", "grh"}},
            {"grm1l_inst.dat", {"-1", "grl"}},
            {"gr1l_inst.dat", {"1", "grl"}},
            {"gr1h_inst.dat", {"1", "grh"}},
            {"gr3h_inst.dat", {"3", "grh"}},
            {"gr3l_inst.dat", {"3", "grl"}},
            {"gr5h_inst.dat", {"5", "grh"}},
            {"gr5l_inst.dat", {"5", "grl"}},
            {"gr5bal_inst.dat", {"5", "gr"}},
            {"gr3bal_inst.dat", {"3", "gr"}},
            {"gr1bal_inst.dat", {"1", "gr"}},
            {"grm1bal_inst.dat", {"-1", "gr"}},
            {"grm3bal_inst.dat", {"-3", "gr"}},
            {"grm5bal_inst.dat", {"-5", "gr"}},
            {"w500str_inst.dat", {"500", "strong corelation"}},
            {"w1000str_inst.dat", {"1000", "strong corelation"}},
            {"w5000str_inst.dat", {"5000", "strong corelation"}},
            {"w10000str_inst.dat", {"10000", "strong corelation"}},
            {"w10000corr_inst.dat", {"10000", "corelation"}},
            {"w5000corr_inst.dat", {"5000", "corelation"}},
            {"w1000corr_inst.dat", {"1000", "corelation"}},
            {"w500corr_inst.dat", {"500", "corelation"}},
            {"w500_inst.dat", {"500", "maxweight"}},
            {"m08_inst.dat", {"0.8", "ratio"}},
            {"m06_inst.dat", {"0.6", "ratio"}}};
    for (auto prefix :prefixes ) {
        cout << "Tested type is " << prefix.first << endl;
        for (auto method: METHODS) {
            cout << "Tested method " << METHOD_PRINT[method] << endl;
            /*for (auto i: INSTANCES) {
                cout << "Current size is " << i << endl;
*/
                auto problem_file_name = "data/" + prefix.first;
                //auto solution_file_name = "data/" + prefix + to_string(i) + SOLUTION_POSTFIX;

                auto problems = readFile(problem_file_name);
                //readSols(solution_file_name, problems);
                ld maxEps = -1, meanEps = 0;
                switch (method) {
                    case BNB: {
                        cout <<"BNB" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.branchBounds();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            p.second.setSolution(s);
                            out << p.second.getId() << "," << prefix.second.second  << "," << prefix.second.first << "," << METHOD_PRINT[method] << ","
                              << dur.count() << "," << 0 << endl;
                        }
                        break;
                    }
                    case GREEDY:

                        maxEps = -1;
                        meanEps = 0;
                        cout <<"GREEd" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.greedyEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            if (s > maxEps)
                                maxEps = s;
                            out << p.second.getId() << "," << prefix.second.second  << "," << prefix.second.first << ","  << METHOD_PRINT[method] << ","
                                 << dur.count() << "," << s << endl;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        cout << endl << "Greedy: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        mistakes << METHOD_PRINT[method] << "," << prefix.second.second  << "," << prefix.second.first << "," << maxEps << "," << meanEps << endl;
                        break;
                    case REDUX:
                        maxEps = -1;
                        meanEps = 0;
                        cout <<"RED" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            start = high_resolution_clock::now();
                            auto s = p.second.reduxEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            if (s > maxEps)
                                maxEps = s;
                            out << p.second.getId() << "," << prefix.second.second  << "," << prefix.second.first << "," << METHOD_PRINT[method] << ","
                                << dur.count() << "," << s << endl;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        cout << endl << "Redux: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        mistakes << METHOD_PRINT[method] << "," << prefix.second.second  << "," << prefix.second.first  << "," << "," << maxEps << "," << meanEps << endl;

                        break;
                    case DYNPROG:
                        maxEps = -1;
                        meanEps = 0;
                        cout <<"DYN" << METHOD_PRINT[method] << endl;
                        for (auto &p: problems) {
                            if(p.first % 100 == 0)
                                cout << p.first;
                            auto s = p.second.dynProgEps();
                            stop = high_resolution_clock::now();
                            dur = duration_cast<microseconds>(stop - start);
                            out << p.second.getId() << "," << prefix.second.second  << "," << prefix.second.first << "," << METHOD_PRINT[method] << ","
                                << dur.count() << "," << s << endl;
                            if (s > maxEps)
                                maxEps = s;
                            meanEps += s;
                        }
                        meanEps /= problems.size();
                        mistakes << METHOD_PRINT[method] << "," << prefix.second.second  << "," << prefix.second.first  << ","  << "," << maxEps << "," << meanEps <<  endl;

                        break;

                    case FPTAS:
                        cout <<"FPTAS" << METHOD_PRINT[method] << endl;
                        for (auto e: ERRORS) {
                            maxEps = -1;
                            meanEps = 0;
                            for (auto &p: problems) {
                                if(p.first % 100 == 0)
                                    cout << p.first;
                                p.second.setFptasSolution(-1);
                                auto s = p.second.fptasEps(e);

                                stop = high_resolution_clock::now();
                                dur = duration_cast<microseconds>(stop - start);
                                out << p.second.getId() << "," << prefix.second.second  << "," << prefix.second.first << "," << METHOD_PRINT[method]
                                    << ","
                                    << dur.count() << "," << s << endl;
                                if (s > maxEps)
                                    maxEps = s;
                                meanEps += s;
                            }
                            meanEps /= problems.size();
                            mistakes << METHOD_PRINT[method] << "," << prefix.second.second  << "," << prefix.second.first  << "," << "," << maxEps << "," << meanEps << "," << e <<  endl;
                            cout << endl << "FPTAS: Max eps " << maxEps << " meanEps " << meanEps << endl;
                        }

                        break;
                    default:
                        cout << "Method not implemented";
                }

            }


    }
    return  0;
}

